"""`main` is the top level module for your Bottle application."""


from google.appengine.ext import ndb

# import the Bottle framework
from bottle import Bottle, request, route, abort, response

# Create an instance of bottle.
bottle = Bottle()

# Data model.
class KeyVal(ndb.Model):
    val = ndb.BlobProperty()


@bottle.route('/read')
@bottle.route('/keystore/read')
def read_data():
    response.set_header('Access-Control-Allow-Origin', '*')
    key = request.query.key
    if key is None: 
        abort(500, "The key is missing")
    k = ndb.Key(KeyVal, key)
    kv = k.get()
    r = None if kv is None else kv.val
    return dict(result=r)


@bottle.route('/store')
@bottle.route('/keystore/store')
@bottle.post('/store')
@bottle.post('/keystore/store')
def write_data():
    response.set_header('Access-Control-Allow-Origin', '*')
    key = request.params.key
    if key is None: 
        abort(500, "The key is missing")
    val = str(request.params.val)
    kv = KeyVal(id=key)
    kv.val = val
    kv.put()
    return "ok"
